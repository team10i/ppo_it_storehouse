import sqlite3
import json
from string import Template
from service_config import SQLITE

_SQL = {
    "init": """\
CREATE TABLE IF NOT EXISTS scheme(
    id TEXT PRIMARY KEY,
    merged_with TEXT
);
CREATE TABLE IF NOT EXISTS warehouse (
    id TEXT,
    name TEXT,
    uuid TEXT,
    unit_size INTEGER,
    item_size INTEGER,
    weight REAL,
    loaded BOOL
);
CREATE TABLE IF NOT EXISTS remote (
    name TEXT
);
CREATE TABLE IF NOT EXISTS checkout (
    name TEXT,
    uuid TEXT,
    checkout_from TEXT,
    FOREIGN KEY(`checkout_from`) REFERENCES `warehouse`(`id`) 
);
""",
    "get_warehouse_list": """\
SELECT * FROM warehouse;
""",
    "add_scheme": """\
INSERT INTO scheme VALUES(?, ?);
""",
    "clear_scheme": """
DELETE FROM scheme
""",
    "get_scheme": """
SELECT * FROM scheme    
""",
    "add_warehouse": """
INSERT INTO warehouse VALUES(?, ?, ?, ?, ?, ?, ?)
""",
    "get_empty_pos_by_size": Template("""
SELECT id FROM warehouse WHERE unit_size = $SIZE AND loaded = 0
"""),
    "add_item_to_unit": Template("""
UPDATE warehouse SET name = '$NAME', uuid = '$UUID', loaded = 1, weight = $WEIGHT, item_size = $ITEM_SIZE
    WHERE id = '$ID';
"""),
    "add_item_to_remote": """
INSERT INTO remote VALUES(?)
""",
    "get_remote_list": """\
SELECT * FROM remote;
""",
    "checkout_1": Template("""\
UPDATE warehouse SET name = NULL, uuid = NULL, loaded = 0, weight = NULL, item_size = NULL
    WHERE id = '$ID';
"""),
    'checkout_2': """\
    INSERT INTO checkout VALUES (?, ?, ?)
""",
    "get_checkout_list": """\
SELECT * FROM checkout
""",
    "get_item_by_unit": Template("""\
SELECT name, uuid FROM warehouse WHERE id = '$ID' 
"""),
}

class DBProvider:
    def __init__(self):
        with sqlite3.connect(SQLITE.get("file")) as conn:
            conn.executescript(_SQL.get("init"))
            conn.commit()

    def get_warehouse_list(self):
        sql =_SQL.get('get_warehouse_list')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            out = conn.execute(sql).fetchall()
            conn.commit()
        out = list(map(list, out))
        for i in range(len(out)):
            out[i][0] = json.loads(out[i][0])
        return out
    
    def add_scheme(self, scheme):
        sql = _SQL.get('add_scheme')
        current_scheme = self._get_scheme()
        if not current_scheme:
            pass
        else: 
            self._clear_scheme()
        with sqlite3.connect(SQLITE.get('file')) as conn:
            for item in scheme:            
                conn.execute(sql, (item[0], json.dumps(item[1])))
            conn.commit()

    def _clear_scheme(self):
        sql = _SQL.get('clear_scheme')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            conn.execute(sql)

    def _get_scheme(self):
        sql = _SQL.get('get_scheme')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            out = conn.execute(sql).fetchall()
        out = list(map(list, out))
        for i in range(len(out)):
            out[i][1] = json.loads(out[i][1])
        return out

    def add_warehouse(self, warehouse):
        sql = _SQL.get('add_warehouse')
        if not self.get_warehouse_list():
            with sqlite3.connect(SQLITE.get('file')) as conn:
                for unit in warehouse:
                    conn.execute(sql, 
                            (json.dumps(unit),
                                None,
                                None,
                                len(unit),
                                None,
                                None,
                                False))
        else:
            pass

    def get_empty_pos_by_size(self, size):
        sql = _SQL.get('get_empty_pos_by_size', Template("")).substitute(SIZE=size)
        with sqlite3.connect(SQLITE.get('file')) as conn:
            out = conn.execute(sql).fetchall()
        out = list(map(lambda x: x[0], out))
        out = list(map(json.loads, out))
        return out

    def add_item_to_unit(self, item, unit, item_size):
        sql = _SQL.get('add_item_to_unit', Template("")).substitute(NAME=item.get('name'),
                UUID=item.get('uuid'),
                ID=json.dumps(unit),
                WEIGHT=item.get('weight'),
                ITEM_SIZE = item_size)
        with sqlite3.connect(SQLITE.get('file')) as conn:
            conn.execute(sql)

    def add_item_to_remote(self, item):
        sql = _SQL.get('add_item_to_remote')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            conn.execute(sql, (item.get('name'),))
            conn.commit()

    def get_remote_list(self):
        sql =_SQL.get('get_remote_list')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            out = conn.execute(sql).fetchall()
            conn.commit()
        out = [{'name': i[0]} for i in out]
        return out

    def checkout(self, unit):
        checkout = self._get_item_by_unit(unit)
        sql = _SQL.get('checkout_1', Template("")).substitute(ID=json.dumps(unit))
        with sqlite3.connect(SQLITE.get('file')) as conn:
            conn.executescript(sql)
            conn.commit()
        sql = _SQL.get('checkout_2')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            conn.execute(sql, (checkout[0],
                checkout[1],
                json.dumps(unit)))
            conn.commit()

    def _get_item_by_unit(self,unit):
        sql = _SQL.get('get_item_by_unit', Template("")).substitute(ID=json.dumps(unit))
        with sqlite3.connect(SQLITE.get('file')) as conn:
            out = conn.execute(sql).fetchall()[0]
        return out

    def get_checkout_list(self):
        sql = _SQL.get('get_checkout_list')
        with sqlite3.connect(SQLITE.get('file')) as conn:
            out = conn.execute(sql).fetchall()
        out = list(map(list, out))
        for i in range(len(out)):
            out[i][2] = json.loads(out[i][2])
        return out

db_provider = DBProvider()
